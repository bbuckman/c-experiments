#include <stdio.h>
#define MAXSIZE 80 //Print lines longer than this
#define MAXLINE 1000 //Prevent overflows?

void printline(char line[], int maxline, int maxsize);


int main() {
	int len; //current line length
	char line[MAXLINE]; //current input line
	
	printline(line, MAXLINE, MAXSIZE);	

	
	return 0;
}

void printline(char line[], int maxline, int maxsize) {
	char c;
	while((c=getchar()) != EOF) {
		int i = 0;
		int j = 0;
		for (i=0; i<maxline-1 && (c=getchar())!=EOF;++i) { 
			if (c != '\n') {
				line[j] = c;
				++j;
			}
			else {
				if (j < maxsize) {	 
					j = 0;
				}
				else if (j >= maxsize) {
					int k=0;
					printf("80+ character line:\n");
					for (k=0;k<=j;++k) {
						printf("%c", line[k]);
						
					}
					printf("\n");
					j = 0;	
				}

			}

		}
	}
}
