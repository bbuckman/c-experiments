#include <stdio.h>

/* print Celsius-Fahrenheit table
	Reverse order */

// K&R Chapter 1.5
void convert(int min, int max, int step);

void convert(int min, int max, int step) {
	float fahr, celsius;

	
	printf("%s\t%s\n", "Celsius Temp","Fahrenheit Temp");
	printf("%s\t%s\n", "------------","---------------");
		
	for (celsius=max;celsius>=min;celsius-=step)
	{
		fahr = ((9.0/5.0) * celsius) +32;
		printf("%3.0f\t\t\t%6.1f\n", celsius, fahr);
	}
}
int main()
{
convert(0, 300, 20);
}

		
