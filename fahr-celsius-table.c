#include <stdio.h>

/* print Farenheit-Celsius table
	for fahr = 0, 20, ..., 300 *; flotaing-point version */

// K&R Chapter 1.2

int main()
{
	float fahr, celsius;
	int lower, upper, step;

	lower = 0;	// lower limit of temperature table
	upper=300;	// upper limit of temperature table;
	step=20;	// step size
	
	fahr=lower;
	while (fahr <= upper)
	{
		celsius = (5.0/9.0) * (fahr -32);
		printf("%3.0f %6.1f\n", fahr, celsius);
		fahr = fahr + step;
	}
}
		
