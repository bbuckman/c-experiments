#include <stdio.h>

#define IN 1 //inside a word
#define OUT 0 //outside a word

int main() {
	char c;
	int state = OUT;
	while ((c=getchar()) != EOF) {
		// putchar(c);
		if (c == '\t' || c== '\n' || c == ' ') {
			state = OUT;
			putchar('\n');
		}
		else {
			state = IN;
			putchar(c);
		}
	}
}
