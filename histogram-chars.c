#include <stdio.h>
#define MAX 123
#define MIN 97
#define CASE 32

int main() {
	
	int charcount[MAX]; 
	int h=0, i=0, j=0;
	int c=0;
	
	// Initial loop to zero out array
	for (h=0;h<MAX;++h) {
		charcount[h] = 0;
	}
	// Loop to get characters and count
	
	while ((c=getchar()) != EOF) {
		if (c >= MAX) {
			c = 0;
		}
		else if (c >= MIN) {
			++charcount[c];
		}
		else {
			++charcount[c+CASE];
		}
	}

	// Loop to print histogram
	for (i=MIN;i<MAX;++i) {
		printf("%c: ", i);
		for (j=0;j<charcount[i];j++) {
			printf("*");
		} 
		
		printf("\n");
	}
	return 0;
}
