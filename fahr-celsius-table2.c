#include <stdio.h>

/* print Farenheit-Celsius table
	for fahr = 0, 20, ..., 300 *; flotaing-point version */

// K&R Chapter 1.2

int main()
{
	float fahr, celsius;
	int lower, upper, step;

	lower = 0;	// lower limit of temperature table
	upper=200;	// upper limit of temperature table;
	step=10;	// step size
	
	celsius=lower;
	while (celsius <= upper)
	{
		fahr = (9.0/5.0) * (celsius + 32);
		printf("%3.0f %6.1f\n", celsius, fahr);
		celsius = celsius + step;
	}
}
		
