#include <stdio.h>

int main() {
	double nb, nt, nn;
	char c;

	while ((c=getchar()) != EOF) {
		if (c == ' ') {
			++nb;
		}
		else if (c == '\t') {
			++nt;
		}
		else if (c == '\n') {
			++nn;
		}
	}
	printf("There were %.0f blanks, %.0f tabs, and %.0f newlines.\n", nb, nt, nn);
}
