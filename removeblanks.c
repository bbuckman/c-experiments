#include <stdio.h>

#define MAXLINE 1000 //Prevent overflows?

void printline(char line[], int maxline);


int main() {
	int len; //current line length
	char line[MAXLINE]; //current input line
	
	printline(line, MAXLINE);	

	
	return 0;
}

void printline(char line[], int maxline) {
	char c;
	
	while((c=getchar()) != EOF) {
		int i = 0;
		int j = 0; //loop value
		int k = 0; //counter of blanks and tabs
		int h = 0; //loop value
		int l = 0; //character counter
		for (i=0; i<maxline-1 && (c=getchar())!=EOF;++i) { 
			
			line[i] = c;
			// printf("%d:  char %c", i, c);	
			if (c != '\n') {
				
				++l;
			}
			else {
				for (j=0;j<=l;++j) {
					if (line[l-k] == '\t' || line[l-k] == ' ') {
					k++;  //counter of blanks, tabs to eliminate
					}
					else {
						j=0;
						break;
					} 
				}	
				
				for (h=i-l;h<(i-k);++h) {
					printf("%c", line[h]);					
				}
				printf("\n");
							
				h=0;
				k=0;
				l=0;
			}

		}
	}
}
