#include <stdio.h>

/* print Celsius-Fahrenheit table
	Reverse order */

// K&R Chapter 1.5

int main()
{
	float fahr, celsius;
	
	printf("%s\t%s\n", "Celsius Temp","Fahrenheit Temp");
	printf("%s\t%s\n", "------------","---------------");
		
	for (celsius=300;celsius>=0;celsius-=20)
	{
		fahr = ((9.0/5.0) * celsius) +32;
		printf("%3.0f\t\t\t%6.1f\n", celsius, fahr);
	}
}
		
