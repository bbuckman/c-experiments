#include <stdio.h>
#define IN 1
#define OUT 0
#define MAXLEN 10

int main() {
	int wl[MAXLEN], nc, h, i, j, state;
	char c;
	
	// Initial loop to zero out array
	for (h=0;h<10;h++) {
		wl[h] = 0;
	}
	// Loop to get characters and count
	nc = 0;
	while ((c=getchar()) != EOF) {
		if (c == '\n' || c == '\t' || c == ' ') {
			state = OUT;
			++wl[nc];
			nc = 0;
		}
		else if (state = OUT) {
			state = IN;
			
		}
		else {
			++nc;
		}
	}

	// Loop to print histogram
	for (i=0;i<10;i++) {
		printf("%d: ", i);
		for (j=0;j<wl[i];j++) {
			printf("*");
		}

		printf("\n");
	}
	return 0;
}
